\label{chap:modelling}

Dieses Kapitel befasst sich mit der Umsetzung der abstrahierten Regeln und Repräsentationen des Spieles in eine Form, die für Reinforcement Learning verwendet werden kann.

\section{Repräsentation des Spielbrettes}

Der Zustand des Spiels \bzw. des Spielbrettes zum Zeitpunkt $\timePoint$ wird als 12-Tupel $\state_\timePoint$ modelliert.
\marginnote{$\activeP \triangleright n$ ist die ID des Spielers der nach $n$ Zügen nach $\activeP$ am Zug ist. Also $a \triangleright b = \rK{a+b \mod 4}$}
\begin{align}
 \label{eqa:stateTupel}
 \state_\timePoint = \eK{
    \isPresentMat,
    \connectivityMat_{\direction_1},
    \ldots,
    \connectivityMat_{\direction_6},
    \valueMat_\timePoint^{\playerId_1}, 
    \valueMat_\timePoint^{\playerId_2}, 
    \valueMat_\timePoint^{\playerId_3}, 
    \valueMat_\timePoint^{\playerId_4}, 
    \valueMat_\timePoint^\neuralId
 } \in \stateSet
\end{align}

Dabei besteht das 12-Tupel aus 12 $\rows \times \cols$ Matrizen.
Die ersten 7 Matrizen bilden die Repräsentation der Spielbrettkonfiguration
\marginnote{$\B = \qK{0,1}$}
\marginnote{$\directionSet$ beinhaltet die Richtungen: oben, rechts, rechts-unten, unten, links, links-oben}
\[
 \isPresentMat, \connectivityMat_\direction \in \B^{\rows \times \cols}\ \forall \direction \in \directionSet
\]
Diese Matrizen sind binäre Matrizen, haben also nur einen Wert von 1 oder 0.
Hierbei wird mit der Matrix $\isPresentMat$ modelliert, welche Zellen in der Spielpartie spielbar sind (siehe \autoref{fib:example_presentsMat} für ein Beispiel).
Dabei gilt für Zelle $\cell$
\marginnote{
    Hier bedeutet $M\eK{\cell} = x$
    dass der Werte in der Matrix $M$ für die Position der Zelle $\cell$ den Wert $x$ hat.
}
\[
 \isPresentMat\eK{\cell} = 
 \begin{cases}
  1  & \text{die Zelle $\cell$ ist spielbar} \\
  0  & \text{die Zelle $\cell$ ist nicht spielbar}
 \end{cases}
\]



\begin{figure}
 \centering
 \[
  \isPresentMat = \begin{pmatrix} 
                    1 & 0 & 0 & 0 & 0 \\
                    0 & 1 & 1 & 0 & 0 \\
                    1 & 1 & 1 & 1 & 0 \\
                    0 & 1 & 1 & 1 & 1 \\
                    0 & 0 & 0 & 1 & 0 
                  \end{pmatrix}
 \]

 \caption{
    Dies ist die Matrix $\isPresentMat$ zu der Spielbrettkonfiguration in \autoref{fig:example_board}
 }
 \label{fib:example_presentsMat}
\end{figure}

Die Matrizen $\connectivityMat_\direction$ geben an, ob eine Zelle mit der Nachbarzelle in Richtung $\direction$ verbunden ist. 
\[
 \connectivityMat_\direction\eK{\cell} = 
    \begin{cases}
     1 & \text{Zelle $\cell$ ist in Richtung $\direction$ mit dem Nachbarn verbunden} \\
     0 & \text{nicht verbunden}
    \end{cases}
\]

Anmerkung: diese Form der Modellierung beinhaltet Redundanzen, da jede Kante des Graphens in je zwei der Matrizen repräsentiert ist.
Diese Form bietet dem neuronalen Netz in Verbindung mit Convolution die Möglichkeit, einfacher Nachbarschaften zu kodieren.

\begin{figure}
 \centering
 
 \[
  \connectivityMat_{\text{rechts-unten}} = 
  \begin{pmatrix}
     1 & 0 & 0 & 0 & 0 \\
     0 & 0 & 0 & 0 & 0 \\
     0 & 1 & 1 & 0 & 0 \\
     0 & 0 & 1 & 0 & 0 \\
     0 & 0 & 0 & 0 & 0 
  \end{pmatrix}, 
  \connectivityMat_{\text{links-oben}} = 
  \begin{pmatrix}
     0 & 0 & 0 & 0 & 0 \\
     0 & 1 & 0 & 0 & 0 \\
     0 & 0 & 0 & 0 & 0 \\
     0 & 0 & 1 & 1 & 0 \\
     0 & 0 & 0 & 1 & 0 
  \end{pmatrix}
 \]

 \caption{
    Beispiel Matrizen für die Verbindungen rechts-unten und links-oben von dem Beispiel Spielbrett aus \autoref{fig:example_board}
 }
 \label{fig:example_connectionMat}
\end{figure}


\subsection{Stärke und Zugehörigkeit}

Die Stärke und Zugehörigkeit der Zellen zum Zeitpunkt $\timePoint$ wird über die Matrizen $\valueMat_\timePoint^{\playerId}$ kodiert, wobei $\playerId$ die ID eines Spielers ist.
\marginnote{Die ID des $n$-ten Spielers ist $n-1$}
Für die Kodierung werden die Stärken der Zellen auf das Intervall $\eK{0,1}$ normalisiert.
Die Stärke einer Zelle wird nur in der Matrix des zugehörigen Spielers eingetragen.

Also gilt für die Zelle $\cell$:
\begin{align}
 &\owner{\cell} = \playerId \myAnd \strength{\cell} = x \\
 \Leftrightarrow \
 &\valueMat_\timePoint^\playerId\eK{\cell} = \frac{x}{9} 
 \myAnd \forall \playerId' \in \playerIdSet \not = \playerId: \valueMat_\timePoint^{\playerId'}\eK{\cell} = 0
\end{align}

Die Kodierung der Reihenfolge der Spieler geschieht hier durch die Anordnung der Matrizen $\valueMat_\timePoint^{\playerId}$ im Spielzustand $\state_\timePoint$.
Diese werden so \textit{rotiert}, dass immer die Matrix des zum Zeitpunkt $\timePoint$ aktiven Spielers links vor den Matrizen der anderen Spielern auftaucht.
Sei $\activeP$ die ID des aktiven Spielers und sei $\activeP \triangleright n$ die ID des Spielers der in $n$ Zügen am Zug ist.
Setzt man nun für die Werte $p_i$ in \autoref{eqa:stateTupel} die Werte $\activeP \triangleright \rK{i-1}$ ein, bekommt man
\begin{align}
 \state_\timePoint = \eK{\ldots, 
        \valueMat_\timePoint^{\activeP},
        \valueMat_\timePoint^{\activeP \triangleright 1},
        \valueMat_\timePoint^{\activeP \triangleright 2},
        \valueMat_\timePoint^{\activeP \triangleright 3},
        \valueMat_\timePoint^\neuralId
 }
\end{align}

\begin{figure}
  \centering
  \newcommand{\nv}[1]{\nicefrac{#1}{9}}
  \begin{gather*}
    \valueMat_\timePoint^0 = 
    \begin{pmatrix}
    0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 \\
    0 & \nv{5} & 0 & 0 & 0 \\
    0 & 0 & 0 & \nv{2} & 0 
    \end{pmatrix},
    \valueMat_\timePoint^1 = 
    \begin{pmatrix}
    0 & 0 & 0 & 0 & 0 \\
    0 & 0 & \nv{2} & 0 & 0 \\
    \nv{7} & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 
    \end{pmatrix}, \\
    \valueMat_\timePoint^2 = 
    \begin{pmatrix}
    0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 \\
    0 & \nv{6} & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & \nv{7} \\
    0 & 0 & 0 & 0 & 0 
    \end{pmatrix},
    \valueMat_\timePoint^3 = 
    \begin{pmatrix}
    \nv{7} & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 1 & \nv{2} & 0 \\
    0 & 0 & \nv{5} & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 
    \end{pmatrix}, \\
    \valueMat_\timePoint^4 = 
    \begin{pmatrix}
    0 & 0 & 0 & 0 & 0 \\
    0 & \nv{5} & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & \nv{7} & 0 \\
    0 & 0 & 0 & 0 & 0 
    \end{pmatrix}
  \end{gather*}

  \caption{
    Die Matrizen $\valueMat_\timePoint^\playerId$ zu dem Spielbrett in \autoref{fig:example_board}.
  }
  \label{fig:example_valueMat}
\end{figure}



\section{Trainingsziele}

Wie bereits in \autoref{chap:basics-RL} erwähnt wird beim Reinforcement-Learning eine \gls{policy} und eine \gls{valueFunction} optimiert.
Da das Spiel aber aus zwei unterschiedlichen Phasen besteht, wird hier nicht nur eine, sondern zwei \glspl{policy} gelernt. 
Diese \glspl{policy} sind $\policyAttack$ und $\policyReinforcement$, welche jeweils für die Angriffs- und Verstärkungs-Phase vorgesehen sind.

Dabei ordnen die \glspl{policy} für einen gegebenen Zustand $\state_\timePoint$ allen Aktionen, die der aktive Spieler im Zustand $\state_\timePoint$ durchführen könnte, eine \gls{prob} zu.
Sei dazu $\actionSet_x\rK{\state_\timePoint}$ die Menge aller Aktionen, die der aktive Spieler im Zustand $\state_\timePoint$ in der Phase $x \in \qK{\attackPhase, \reinforcementPhase}$ durchführen kann.
Dabei ist dann 
\begin{align}
 \policy_x\rK{y|\state_\timePoint}
\end{align}
die \gls{prob}, dass der aktive Spieler die Aktion $y$ durchführen würde.

Die \glspl{policy} geben also \gls{probDist} über den möglichen Aktionen an.

Dabei besteht $\actionSet_\attackPhase\rK{.}$, also die Menge der Aktionen in der Angriffs-Phase, immer aus einer Pass-Aktion und einer oder mehren Angriffs-Aktionen.
Eine Angriffs-Aktion ist ein Tupel $\eK{\cell, \direction}$, wobei $\cell$ die Zelle ist, von der aus angegriffen werden soll und $\direction$ die Richtung, in die angegriffen werden soll.

Die Menge $\actionSet_\reinforcementPhase\rK{.}$, also die Menge der Aktionen in der Verstärkungs-Phase, enthält ebenfalls immer eine Pass-Aktion und zusätzlich eine oder mehrere Verstärkungs-Aktionen.
Eine Verstärkungs-Aktion beinhaltet lediglich eine Zelle $\cell$, die verstärkt werden soll.

Wie in \autoref{chap:basics-RL} schon erläutert wurde, ist die \gls{valueFunction} eine Schätzung für die Summe der zukünftigen \glspl{reward}. 
Diese Schätzung wird einfacher, wenn jede Aktion mit Ausnahme der Aktion, die das Spiel beendet, keine \gls{reward} liefert.
Dann ist die \gls{valueFunction} alleine bestimmbar aus dem Endzustand des Spielbrettes.

Diese End-\gls{reward} wird definiert über die Anzahl der Zellen, die ein Spieler kontrolliert, relativ zu der Anzahl der Zellen, die nicht neutral sind.
\marginnote{
  Zellen die neutral sind werden für die Berechnung nicht beachtet, da sie keine \quot{Bedrohung} für den Spieler darstellen
}

Seien $\state_E$ der Endzustand des Spieles, $n = \bK{\qK{\cell \in \cellSet | \owner{\cell} \not= \neuralId}}$ die Anzahl der Zellen, die nicht neutral sind und 
\[
n_\playerId = \bK{\qK{\cell \in \cellSet | \owner{\cell} = \playerId}}
\] 
die Anzahl der Zellen, die der Spieler mit der ID $\playerId$ kontrolliert, dann ist 
\[
 x_\playerId = n_\playerId / n
\]
die relative Anzahl der Zellen, die der Spieler $\playerId$ kontrolliert.
Dieser Wert liegt, da er ein Prozentwert ist, im Intervall $\eK{0,1}$. 
Da wir aber schlechtes Spielen bestrafen wollen, wird der Wert $x_\playerId$ mit der Funktion
\[
 f\rK{x} = 2 x^{\dfrac{\ln\rK{\nicefrac{1}{2}}}{\ln{\nicefrac{1}{\numberOfPlayers}}}} - 1
\]
umgewandelt, wobei $\numberOfPlayers$ die Anzahl der Spieler ist.
Die \gls{reward} $\reward_\playerId$ des Spielers $\playerId$ ist dann 
\begin{align}
  \reward_\playerId = f\rK{x_\playerId} \label{eqn:reward_function}
\end{align}

Die Funktion $f$ ist so gewählt dass die \gls{reward} immer im Intervall $\eK{-1,1}$ liegt und dass $\reward_\playerId > 0$ ist, wenn der Spieler $\playerId$ mehr als $\nicefrac{1}{\numberOfPlayers}$ der nicht neutralen Zellen kontrolliert, $\reward_\playerId < 0$ ist, wenn er weniger als $\nicefrac{1}{\numberOfPlayers}$ der nicht neutralen Zellen kontrolliert und $\reward_\playerId = 0$ ist, wenn er genau $\nicefrac{1}{\numberOfPlayers}$ der nicht neutralen Zellen kontrolliert.
\begin{figure}[b]
  \centering
  \begin{tikzpicture}
    \begin{axis}[
        axis lines=middle,
        xlabel=$x_\playerId$,
        ylabel=$\reward_\playerId$,
        enlargelimits,
        xmin = 0, xmax = 1,
        ymin = -1, ymax = 1,
        xtick={0,0.25,0.5,0.75,1},
        ytick={-1,-0.5,0,0.5,1}
      ]
      \addplot[domain=0:1, samples=1000, color=red] {2*(x)^(1/2) - 1};
    \end{axis}
  \end{tikzpicture}

  \caption{
    Graph für die \gls{reward} $\reward_\playerId$ für die relative Anzahl der kontrollierten Zellen $x_\playerId$ des Spielers $\playerId$ für ein Spiel mit vier Spielern ($\numberOfPlayers = 4$). 
    Man beachte, dass die Funktion $f$ für $\numberOfPlayers = 4$ sich zu $f\rK{x} = 2 \sqrt{x} -1$ vereinfachen lässt.
  }
  \label{fig:reward_plot}
\end{figure}
Also gilt:
\begin{align}
  x_\playerId < \frac{1}{\numberOfPlayers} &\Leftrightarrow \reward_\playerId < 0 \\
  x_\playerId = \frac{1}{\numberOfPlayers} &\Leftrightarrow \reward_\playerId = 0 \\
  x_\playerId > \frac{1}{\numberOfPlayers} &\Leftrightarrow \reward_\playerId > 0 
\end{align}

Mit dieser \gls{reward} ist dann die optimale \gls{valueFunction}
\[
  \mvalue_\playerId^*\rK{\state_\timePoint} = \reward_\playerId\rK{\state_E}
\]
wobei $\state_E$ der für Spieler $\playerId$ beste Endzustand ist, der vom Zustand $\state_\timePoint$ bei optimalem Spiel erreicht werden kann.

Die optimale \gls{valueFunction} $\mvalue_\playerId^*\rK{\state_\timePoint}$ muss dann mittels Reinforcement-Learning als $\mvalue_\playerId\rK{\state_\timePoint}$ gelernt werden.

\section{Architektur des neuronalen Netzes}
Für die Versuche, die in \autoref{chap:training} erläutert werden, wurde ein neuronales Netz entworfen.
Dabei wurden die Hyperparameter Zeilenanzahl $\rows$ und Spaltenanzahl $\cols$ auf die Werte
\begin{align}
  \rows = 10 \\
  \cols = 10 
\end{align}
festgelegt.
Zudem wurde die Spieleranzahl $\numberOfPlayers$ auf den Wert $\numberOfPlayers = 4$ festgelegt.

Das neuronale Netz besteht aus 4 Komponenten: einem Turm $\tower$ und 3 Köpfen.
Diese sind ein Kopf $\valueHead$ für die \gls{valueFunction}, ein Kopf $\attackHead$ für die Angriffs-\gls{policy} und ein Kopf $\reinforcementHead$ für die Verstärkungs-\gls{policy}.

Dabei besteht der Turm $\tower$ aus einem Convolution-Block $\convBlock$ und 15 Residual-Blöcken $\resBlock$.
Ein Convolution-Block $\convBlock$ folgendermaßen aufgebaut:
\begin{enumerate}
 \item Convolution-Schicht mit 256 Feature-Maps, einem $5,5$ Kernel und einem Stride von 1
 \item gefolgt von einer Batchnormalisierung
 \item gefolgt von einer ReLU-Funktion
\end{enumerate}
\begin{figure}
  \centering
  \begin{tikzpicture}[node distance = 0.6cm, auto, scale=0.8]
    \node (in1) { };
    \node[draw, below=of in1] (in2) {Convolution-Schicht};
    \node[draw, below=of in2] (layer1) {Batchnormalisierung};
    \node[below=of layer1] (out) {};

    \path[->]
    (in1) edge (in2)
    (in2) edge (layer1)
    (layer1) edge node[right] {ReLU} (out);
  \end{tikzpicture}
  \caption{Aufbau eines Convolution-Blockes}
\end{figure}


Ein Residual-Block $\resBlock$ besteht dabei aus zwei Convolution-Blöcken $\convBlock_1$ und $\convBlock_2$ wobei die Ausgabe von $\convBlock_1$ die Eingabe für $\convBlock_2$ ist.
Dabei wird vor der ReLU-Funktion von $\convBlock_2$ die Ausgabe von $\convBlock_1$ komponentenweise zu der Ausgabe von der Batchnormalisierung von $\convBlock_2$ hinzu addiert.
\begin{figure}
\centering
  \begin{tikzpicture}[node distance = 0.6cm, auto, scale=0.8]
    \node (in1) { };
    \node[draw, below=of in1] (in2) {$\convBlock_1$};
    \node[draw, below=of in2] (layer1) {$\convBlock_2$};
    \node[draw, circle, below=of layer1] (add) {$+$};
    \node[below=of add] (out) {};

    \path[->]
    (in1) edge (in2)
    (in2) edge (layer1)
    (layer1) edge (add)
    (add) edge node[right] {ReLU} (out)
    (in2) edge[bend left=90] node [right] {Identität} (add);
  \end{tikzpicture}
  \caption{Aufbau eines Residual-Blockes}
\end{figure}

\pagebreak
Der Kopf für die \gls{valueFunction} ist wie folgt aufgebaut:
\begin{enumerate}
 \item Convolution-Schicht mit einer Feature-Map
 \item gefolgt von einer Batchnormalisierung
 \item gefolgt von einer ReLU-Funktion
 \item gefolgt von einer vollvernetzten Schicht mit 256 \glspl{Unit}
 \item gefolgt von einer Schicht mit einer \gls{Unit} und $\tanh$ als Aktivierungsfunktion
\end{enumerate}
Für den Kopf der \gls{valueFunction} wird der MSE als \gls{loss} verwendet.

Der Kopf der Angriffs-\gls{policy} ist wie folgt aufgebaut:
\begin{enumerate}
 \item Convolution-Schicht mit 2 Feature-Maps
 \item Batchnormalisierung
 \item ReLU-Funktion
 \item vollvernetzte Schicht mit $601=\rK{\numberOfPlayers + 1}\rows \cdot \cols + 1$ \glspl{Unit} und der Softmax-Aktivierungsfunktion
\end{enumerate}
Für den Kopf der Angriffs-\gls{policy} wird die Cross-Entropy als \gls{loss} verwendet.

\marginnote{
  Hier bedeutet \quot{alle denkbaren Aktionen}, dass auch nicht legale Aktionen eine \gls{prob} größer 0 haben können. Diese werden vom Agenten gefiltert und die \gls{prob} für die legale Aktionen werden wieder auf 1 normalisiert.
}
Der Kopf der Verstärkungs-\gls{policy} ist aufgebaut wie $\attackHead$, der einzige Unterschied ist, dass die letzte Schicht nicht 601, sondern $101 = \rows \cdot \cols + 1$ \glspl{Unit} hat.

Bei den beiden Köpfen $\attackHead$ und $\reinforcementHead$ sind die Ausgabewerte jeweils eine \gls{probDist} über alle denkbaren Aktionen für die jeweilige Phase.

Die vier Komponenten $\tower,\valueHead,\attackHead,\reinforcementHead$ werden dann zu einem Netzwerk zusammen gesetzt; dabei ist die Ausgabe von $\tower$ die Eingabe für jeden Kopf $\valueHead,\attackHead,\reinforcementHead$.
\begin{figure}
  \centering
  \begin{tikzpicture}[node distance = 0.6cm, auto, scale=0.8]
    \node[draw] (valueHead) {$\valueHead$};
    \node[draw, right=of valueHead] (attackHead) {$\attackHead$};
    \node[draw, right=of attackHead] (reinforcementHead) {$\reinforcementHead$};
    \node[draw, above=of attackHead] (tower) {$\tower$};
    
    \path[->]
      (tower) edge (valueHead)
      (tower) edge (attackHead)
      (tower) edge (reinforcementHead);
  \end{tikzpicture}
  \caption{Aufbau des neuronalen Netzes}
  \label{fig:network}
\end{figure}

\subsection{Netzwerkvariation}
\label{chap:network_variation}
Zusätzlich wurde für die Versuche in \autoref{chap:training} eine Variation des Netzwerkes verwendet.
Silver \etal haben für die Entwicklung von AlphaGo Zero ein Netzwerk verwendet, wie es oben beschrieben wurde: 
mehrere Köpfe (also Ausgabenetzwerke), die sich einen Turm aus Convolution-Schichten und Residual-Blöcken teilen. Dies ist eine Änderung zu der vorherigen Version AlphaGo, bei der komplett separate Netzwerke verwendet wurden \cite{silver_mastering_2016, silver_mastering_2017}.

Um zu evaluieren, ob es eine Performance Verbesserung gibt, wenn sich die Köpfe einen Turm zur Vorverarbeitung teilen, wird eine Netzwerkvariation getestet, in der jeder Kopf einen eigenen Turm hat.
\begin{figure}
  \centering
  \begin{tikzpicture}[node distance = 0.6cm, auto, scale=0.8]
    \node[draw] (valueHead) {$\valueHead$};
    \node[draw, right=of valueHead] (attackHead) {$\attackHead$};
    \node[draw, right=of attackHead] (reinforcementHead) {$\reinforcementHead$};
    
    \node[draw, above=of valueHead] (towerV) {$\tower_\valueHead$};
    \node[draw, above=of attackHead] (towerA) {$\tower_\attackHead$};
    \node[draw, above=of reinforcementHead] (towerR) {$\tower_\reinforcementHead$};
    
    \path[->]
      (towerV) edge (valueHead)
      (towerA) edge (attackHead)
      (towerR) edge (reinforcementHead);
  \end{tikzpicture}
  \caption{Die Variation des Netzwerks}
  \label{fig:network_variation}
\end{figure}












