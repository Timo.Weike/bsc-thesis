HAS_LATEXMK := $(shell command -v latexmk 2> /dev/null)

TEX = max_print_line=1048576 pdflatex -interaction=nonstopmode
MAIN = thesis
BIB = biber
IDX = makeglossaries

.PHONY: all clean

all: $(MAIN).pdf

$(MAIN).pdf: *.tex *.bib
ifdef HAS_LATEXMK
	latexmk $(MAIN)
else
	echo "First pdflatex run"
	$(TEX) $(MAIN)
	echo "Second pdflatex run"
	$(TEX) $(MAIN)
	echo "Biber run"
	$(BIB) $(MAIN)
	echo "glossarie run"
	$(IDX) $(MAIN)
	echo "Third pdflatex run"
	$(TEX) $(MAIN)
	echo "Fourth pdflatex run"
	$(TEX) $(MAIN)
endif

clean:
	rm -f *~ *.fls *.fdb_latexmk *.pdf *.aux *.log *.glg *.glo *.gls *.ist *.out *.tdo *.toc *.bbl *.bcf *.blg *.run.xml *.backup *.acn *.acr *.alg

